Texo IT Test - API
> Simple test suite based on users CRUD endpoints.

## Configuration

* To compile the classes and run the tests, you must have Java 8+ JDK installed and PATH correct;
* This project is based on Maven, so you must have it installed on your computer, with the PATH to the bin correctly;
* To generate the reports, we must have the allure on the PATH too, same thing as the Maven;
* After this, just go to the source folder where we have the pom.xml and execute the next steps.


## Description

To run the test suite, just need to be in the root folder where is the pom.xml and run the following command:

```sh
mvn test -Dgroups="functional"
```

After we run any of the test suite, the results keep stored on "allure-results", to see them, we must run:
```sh
allure serve allure-results
```

## Tests Validations

The endpoint /users is not accepting an put request to modify the information, the test case responsible for test this functionality was disabled.

```sh
@Disable(FIX xxxxxx)
```
