package data;

import com.github.javafaker.Faker;
import models.User;
import models.UserBuilder;
import java.util.Arrays;
import java.util.List;

public class JsonPlaceHolderDataFactory {


    public String getName() {
        List<String> getName =
                Arrays.asList(
                "alias odio sit",
                "quo vero reiciendis velit similique earum",
                "odio adipisci rerum aut animi"
        );
        return getName.get(0);
    }

    public String getEmail() {
        List<String> getEmail =
                Arrays.asList(
                        "Lew@alysha.tv",
                        "Jayne_Kuhic@sydney.com",
                        "Nikita@garfield.biz"
                );
        return getEmail.get(0);
    }

    public User validUser() {
        return newUser();
    }

    private User newUser() {
        return new UserBuilder().
                build();
    }
}
