
package tests;

import base.Config;
import data.JsonPlaceHolderDataFactory;
import org.junit.jupiter.api.BeforeAll;

public abstract class JsonPlaceHolderConfig extends Config {

    protected static JsonPlaceHolderDataFactory jsonPlaceHolderDataFactory;

    @BeforeAll
    static void setUpBase() {
        jsonPlaceHolderDataFactory = new JsonPlaceHolderDataFactory();
    }
}
