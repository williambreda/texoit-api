
package tests;

import io.restassured.http.ContentType;
import models.User;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.CoreMatchers.is;
import static suite.SuiteTags.FUNC;

class JsonPlaceHolderFunctionalTest extends JsonPlaceHolderConfig {

    @Test
    @Tag(FUNC)
    @DisplayName("Must get a comment based on name search.")
    void getComment() {
        Assert.assertEquals(given().
            queryParam("name", jsonPlaceHolderDataFactory.getName()).
        when().
            get("/comments/").
        then().
            statusCode(SC_OK).
            extract().path("email").toString().replace("[","").replace("]",""), jsonPlaceHolderDataFactory.getEmail());

    }

    @Test
    @Tag(FUNC)
    @DisplayName("Must create a user with random values.")
    void createUser() {
        User user = jsonPlaceHolderDataFactory.validUser();

        given().
            contentType(ContentType.JSON).
            body(user).
        when().
            post("/users").
        then().
            statusCode(SC_CREATED).
            assertThat().body("id",is(11)).
            log().body();

    }

    @Test
    @Tag(FUNC)
    @Disabled("This request is not working")
    @DisplayName("Must change data of the user selected.")
    void changeUser() {
        User user = jsonPlaceHolderDataFactory.validUser();

        user.setEmail("teste@mail.com");
        user.getAddress().getGeo().setLat("12345");
        user.getAddress().getGeo().setLng("54321");

        given().
            contentType(ContentType.JSON).
            body(user).
        when().
            put("/users").
        then().
            statusCode(SC_OK).
            body("email", is("teste@mail.com"),
            "address.geo.lat",is ("12345"),
                    "address.geo.lng", is("54321"));

    }
}

