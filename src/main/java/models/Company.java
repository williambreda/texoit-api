package models;

import com.github.javafaker.Faker;

public class Company{

	Faker faker = new Faker();

	private String name;
	private String catchPhrase;
	private String bs;

	public Company(){
		this.name = faker.company().name();
		this.catchPhrase = faker.company().catchPhrase();
		this.bs = faker.company().bs();
	}

	public void setBs(String bs){
		this.bs = bs;
	}

	public String getBs(){
		return bs;
	}

	public void setCatchPhrase(String catchPhrase){
		this.catchPhrase = catchPhrase;
	}

	public String getCatchPhrase(){
		return catchPhrase;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}
}