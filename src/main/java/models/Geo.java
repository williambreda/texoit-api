package models;

import com.github.javafaker.Faker;

public class Geo{

	Faker faker = new Faker();

	private String lng;
	private String lat;

	public Geo(){
		this.lat = faker.address().latitude();
		this.lng = faker.address().longitude();
	}

	public void setLng(String lng){
		this.lng = lng;
	}

	public String getLng(){
		return lng;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}
}