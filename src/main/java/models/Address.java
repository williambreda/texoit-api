package models;

import com.github.javafaker.Faker;

public class Address{

	Faker faker = new Faker();

	private String street;
	private String suite;
	private String city;
	private String zipcode;
	private Geo geo;

	public Address(){
		this.street = faker.address().secondaryAddress();
		this.suite = faker.address().buildingNumber();
		this.city = faker.address().city();
		this.zipcode = faker.address().zipCode();
		this.geo = new Geo();
	}

	public String getSuite() {
		return suite;
	}

	public void setSuite(String suite) {
		this.suite = suite;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Geo getGeo() {
		return geo;
	}

	public void setGeo(Geo geo) {
		this.geo = geo;
	}


}